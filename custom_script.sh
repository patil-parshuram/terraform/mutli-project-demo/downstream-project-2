#!/bin/bash
upstream_branch="master"
if [[ ! -z "$UP_STREAM_REF_NAME" ]]
then
    upstream_branch=${UP_STREAM_REF_NAME}
fi
echo "Cloning ${upstream_branch} of Upstream project"
git clone -b ${upstream_branch} https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/mutli-project-demo/upstream-project.git
mv ./upstream-project/common.tf ./common.tf

